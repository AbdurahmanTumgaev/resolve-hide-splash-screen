﻿#NoEnv
#SingleInstance, force

; ===========================
; Author: Abdurahman Tumgaev
; Description: Runs DaVinci Resolve in a hidden state. Project Manager is a different window, meaning we do not have to unhide anything.
; If help is needed, ask me for help, no matter the problem, I don't bite ;) (click create new issue and describe the problem): https://gitlab.com/AbdurahmanTumgaev/resolve-hide-splash-screen/-/issues
; 
; ---------------------- Instructions ----------------------
; Get ahk: https://www.autohotkey.com/
; Start this script with ahk.
; If resolve doesnt start, then set the correct path to the Resolve. (Do not delete the "" symbols)
; ===========================


; Change this path:               ↓↓↓↓↓↓↓↓↓↓↓↓↓↓ Do not delete the "" ↓↓↓↓↓↓↓↓↓↓↓↓↓↓
global directoryPathToResolve := "C:\Program Files\Blackmagic Design\DaVinci Resolve" "\Resolve.exe"
; Change this path:               ↑↑↑↑↑↑↑↑↑↑↑↑↑↑ Do not delete the "" ↑↑↑↑↑↑↑↑↑↑↑↑↑↑


HandleResolveStartup(), return
HandleResolveStartup()
{
    if WinExist("ahk_exe Resolve.exe")
        return

    Run, %directoryPathToResolve%

    loop
    {
        if WinExist("ahk_exe Resolve.exe")
            break
    }

    WinGet, splashScreenId, Id, ahk_exe Resolve.exe
    WinHide, ahk_id %splashScreenId%
	
    return
}
