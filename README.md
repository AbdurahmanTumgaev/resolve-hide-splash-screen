# Setup
1. Get ahk: https://www.autohotkey.com/
2. Get & Start script with ahk: https://gitlab.com/AbdurahmanTumgaev/resolve-hide-splash-screen/-/raw/main/ResolveHideSplashScreen.ahk?inline=false
3. Resolve should start after the script was started.




# If Resolve doesnt start:
Open the script with an text editor. You will see a path that is marked with arrows. Open your file explorer and find the folder, that the "Resolve.exe" is in. Replace the existing path (which is marked with the arrows) with the path that "Resolve.exe" is in !(Do not delete the "" symbols)!. Once you start the script with AutoHotKey, it should start DaVinci Resolve and hide it's splashscreen.
If it still doesn't work, describe your problem in issue, don't worry, I don't bite ;): https://gitlab.com/AbdurahmanTumgaev/resolve-hide-splash-screen/-/issues
